FROM sebp/elk

ENV LOGSTASH_START=0

RUN apt-get update
RUN apt-get -y install \
  git \
  python \
  python-pip
RUN pip install --upgrade pip

WORKDIR /home
RUN git clone https://github.com/cwurm/elastic-nyc-taxi-demo.git
RUN git clone https://matejcermak-notix@bitbucket.org/matejcermak-notix/nyc-taxi-elk-docker.git

COPY files/es-template.json /home/elastic-nyc-taxi-demo/
COPY files/logstash.conf /home/elastic-nyc-taxi-demo/
COPY files/elasticsearch.yml /etc/elasticsearch/
COPY files/predict.py /home/elastic-nyc-taxi-demo/
COPY files/requirements.txt /home/elastic-nyc-taxi-demo/
COPY files/start.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/start.sh

WORKDIR /home/elastic-nyc-taxi-demo/
RUN pip install -r requirements.txt
