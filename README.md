# nyc-taxi-elk-docker #
Docker configuration for ELK image [elk-docker](https://hub.docker.com/r/sebp/elk/), used for presentation [elastic-nyc-demo](https://github.com/cwurm/elastic-nyc-taxi-demo)
## Setup ##
run ```sudo ./run.sh```
### Kibana ###
Add indices ```nyc_taxi-2014-11``` and ```nyc_taxi-predict``` (without wildcards). Then go to Settings > Objects and import ```kibana-visualizations.json``` and ```kibana-dashboards.json```.