#!/bin/bash
sysctl -w vm.max_map_count=262144
docker-compose down -v --rmi 'all'
docker-compose pull
docker-compose build --no-cache elasticsearch/elk-presentation
docker-compose up --force-recreate --remove-orphans
